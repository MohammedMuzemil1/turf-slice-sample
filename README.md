# angular.json add this script

"scripts": [
              "src/assets/turf-slice.min.js"
            ]

# app.components.ts
```
declare namespace turfSlice {
  function polygonSlice(polygon: any, linestring: any): any;
}
```

# and some code on the constructor
```
  var polygon = {
      "geometry": {
        "type": "Polygon",
        "coordinates": [[
            [0, 0],
            [0, 10],
            [10, 10],
            [10, 0],
            [0, 0]
        ]]
      }
    };
    var linestring =  {
        "type": "Feature",
        "properties": {},
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [5, 15],
            [5, -15]
          ]
        }
      }
    turfSlice.polygonSlice(polygon, linestring);
```