import { Component } from '@angular/core';

declare namespace turfSlice {
  function polygonSlice(polygon: any, linestring: any): any;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-project';
  constructor(){
    var polygon = {
      "geometry": {
        "type": "Polygon",
        "coordinates": [[
            [0, 0],
            [0, 10],
            [10, 10],
            [10, 0],
            [0, 0]
        ]]
      }
    };
    var linestring =  {
        "type": "Feature",
        "properties": {},
        "geometry": {
          "type": "LineString",
          "coordinates": [
            [5, 15],
            [5, -15]
          ]
        }
      }
    turfSlice.polygonSlice(polygon, linestring);
  }
}
